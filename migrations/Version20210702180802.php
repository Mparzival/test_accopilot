<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210702180802 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE station (id INT AUTO_INCREMENT NOT NULL, geo_point2d VARCHAR(255) NOT NULL, geo_shape VARCHAR(255) NOT NULL, nom VARCHAR(255) NOT NULL, num_station INT NOT NULL, nb_bornettes INT NOT NULL, en_service TINYINT(1) NOT NULL, street VARCHAR(255) NOT NULL, mot_directeur VARCHAR(255) NOT NULL, no VARCHAR(255) NOT NULL, nrivoli VARCHAR(255) NOT NULL, commune VARCHAR(255) NOT NULL, code_insee VARCHAR(5) NOT NULL, location2016 VARCHAR(255) NOT NULL, location2019 VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE `user` (id INT AUTO_INCREMENT NOT NULL, identifiant VARCHAR(180) NOT NULL, roles JSON NOT NULL, password VARCHAR(255) NOT NULL, UNIQUE INDEX UNIQ_8D93D649C90409EC (identifiant), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP TABLE station');
        $this->addSql('DROP TABLE `user`');
    }
}
