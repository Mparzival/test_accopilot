
let map = L.map('map').setView([43.604652, 1.444209], 13);

L.tileLayer('//{s}.tile.openstreetmap.fr/osmfr/{z}/{x}/{y}.png', {
    attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors',
    maxZoom: 18,
    tileSize: 512,
    zoomOffset: -1,
}).addTo(map)


function addMarker(lat, lng, numstation) {
    L.popup({
        autoClose: false,
        closeOnEscape: false,
        closeOnClick: false,
        closeButton: false,
        className: 'marker',
        maxWidth: 400
    })
        .setLatLng([lat, lng])
        .setContent(numstation)
        .openOn(map)
}

Array.from(document.querySelectorAll('.js-marker')).forEach((item) =>{
    addMarker(item.dataset.lat,item.dataset.lng, item.dataset.numstation)
})

$('#map').hide();

changeMapAndListe();

function changeMapAndListe(){
    $('#listeStation').on('click', function(){
        $('#liste').show()
        $('#map').hide()
    })

    $('#mapStation').on('click', function(){
        $('#map').show()
        $('#liste').hide()
    })
}




