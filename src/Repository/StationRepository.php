<?php

namespace App\Repository;

use App\Entity\Station;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Station|null find($id, $lockMode = null, $lockVersion = null)
 * @method Station|null findOneBy(array $criteria, array $orderBy = null)
 * @method Station[]    findAll()
 * @method Station[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class StationRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Station::class);
    }

    /**
     *  Cette query nous remonte la recherche de station
     * @param string $value
     * @return int|mixed|string
     */
    public function findByName(string $value)
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.nom LIKE :val')
            ->setParameter('val', '%' . $value . '%')
            ->orderBy('s.nom', 'ASC')
            ->getQuery()
            ->getResult();
    }
}
