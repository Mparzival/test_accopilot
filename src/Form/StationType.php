<?php

namespace App\Form;

use App\Entity\Station;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class StationType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('geoPoint2d')
            ->add('geoShape')
            ->add('nom')
            ->add('numStation')
            ->add('nbBornettes')
            ->add('enService')
            ->add('street')
            ->add('motDirecteur')
            ->add('no')
            ->add('nrivoli')
            ->add('commune')
            ->add('codeInsee')
            ->add('location2016')
            ->add('location2019')
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Station::class,
        ]);
    }
}
