<?php

namespace App\Controller;

use App\Repository\StationRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;


class HomeController extends AbstractController
{

    /**
     * @var StationRepository
     */
    private StationRepository $stationRepository;

    public function __construct(StationRepository $stationRepository)
    {

        $this->stationRepository = $stationRepository;
    }

    /**
     * @Route("/", name="home")
     * @return Response
     */
    public function index(): Response
    {
        $stations = $this->stationRepository->findAll();
        return $this->render('home/index.html.twig', [
            'stations' => $stations
        ]);
    }

    /**
     * @Route("/search-station", name="search_station", methods={"GET"})
     * @param Request $request
     * @return Response
     */
    public function getStationSearch(Request $request): Response
    {
        $result = null;
        $stationSearch = $request->get('stationSearch');
        if ($stationSearch) {
            $result = $this->stationRepository->findByName($stationSearch);
        } else {
            $result = $this->stationRepository->findAll();
        }
        return $this->render('home/index.html.twig', [
            'stations' => $result
        ]);
    }


    /**
     * @Route("/map", name="map")
     * @return Response
     */
    public function getMap()
    {
        $stations = $this->stationRepository->findAll();
        return $this->render('home/map.html.twig', [
            'stations' => $stations
        ]);
    }
}
