<?php

namespace App\Command;

use App\Entity\Station;
use App\Repository\StationRepository;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\Serializer\Encoder\XmlEncoder;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Serializer\Encoder\CsvEncoder;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;

/**
 * @ORM\Embedded
 */
class ImportCsvCommand extends Command
{
    private SymfonyStyle $io;
    protected static $defaultName = 'app:import-csv';
    protected static $defaultDescription = 'Importer les données depuis un fichier CSV';
    private EntityManagerInterface $em;
    private string $dataDirectory;
    private StationRepository $stationRepo;

    public function __construct(EntityManagerInterface $em, string $dataDirectory, StationRepository $stationRepo)
    {
        parent::__construct();
        $this->dataDirectory = $dataDirectory;
        $this->em = $em;
        $this->stationRepo = $stationRepo;
    }

    protected function configure(): void
    {
        $this
            ->setDescription(self::$defaultDescription)
        ;
    }

    protected function initialize(InputInterface $input, OutputInterface $output): void
    {
        $this->io = new SymfonyStyle($input, $output);
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $this->createStations();

        return 0;
    }

    public function getDataFromFile(): array
    {
        $inputFile = $this->dataDirectory . 'station-velo-toulouse.csv';

        $fileExtension = pathinfo($inputFile, PATHINFO_EXTENSION);

        $normalizers = [new ObjectNormalizer()];

        $encoders = [
            new CsvEncoder(),
            new XmlEncoder()
        ];

        $serializer = new Serializer($normalizers, $encoders);

        /** @var string $fileString */
        $fileString = file_get_contents($inputFile);

        $data = $serializer->decode($fileString, $fileExtension);
        return $data;
    }

    private function createStations(): void
    {
        $this->io->section('Création des stations');

        $stationsCreated = 0;

        $tabDataFromFile = $this->getDataFromFile();

        foreach ($tabDataFromFile as $row){
                if (array_key_exists('nom', $row) && !empty($row['nom'])){
                    $station = $this->stationRepo->findOneBy([
                        'nom' => $row['nom'],
                    ]);

                    if(!$station){
                        $station = new Station();

                        $station->setGeoPoint2d($row['geo_point_2d'])
                            ->setGeoShape($row['geo_shape'])
                            ->setNom($row['nom'])
                            ->setNumStation($row['num_station'])
                            ->setNbBornettes($row['nb_bornettes'])
                            ->setEnService(true)
                            ->setStreet($row['street'])
                            ->setMotDirecteur($row['mot_directeur'])
                            ->setNo($row['no'])
                            ->setNrivoli($row['nrivoli'])
                            ->setCommune($row['commune'])
                            ->setCodeInsee($row['code_insee'])
                            ->setLocation2016($row['locations_2016'])
                            ->setLocation2019($row['locations_2019']);

                        $this->em->persist($station);

                        $stationsCreated ++;
                    }
                }
            }

        $this->em->flush();

        if($stationsCreated > 1){
            $string = "{$stationsCreated} Stations créés en base de données.";
        } elseif ($stationsCreated === 1){
            $string = "1 station a été créée en base de donnée.";
        } else {
            $string = 'Aucune station créée';
        }

        $this->io->success($string);
    }


}
