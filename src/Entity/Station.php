<?php

namespace App\Entity;

use App\Repository\StationRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=StationRepository::class)
 */
class Station
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $geoPoint2d;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $geoShape;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $nom;

    /**
     * @ORM\Column(type="integer")
     */
    private $numStation;

    /**
     * @ORM\Column(type="integer")
     */
    private $nbBornettes;

    /**
     * @ORM\Column(type="boolean")
     */
    private $enService;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $street;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $motDirecteur;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $no;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $nrivoli;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $commune;

    /**
     * @ORM\Column(type="string", length=5)
     */
    private $codeInsee;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $location2016;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $location2019;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getGeoPoint2d(): ?string
    {
        return $this->geoPoint2d;
    }

    public function setGeoPoint2d(string $geoPoint2d): self
    {
        $this->geoPoint2d = $geoPoint2d;

        return $this;
    }

    public function getLat()
    {
        $lat = explode(",",$this->geoPoint2d);
        return $lat[0];
    }

    public function getLng()
    {
        $lng = explode(",",$this->geoPoint2d);
        return $lng[1];

    }

    public function getGeoShape(): ?string
    {
        return $this->geoShape;
    }

    public function setGeoShape(string $geoShape): self
    {
        $this->geoShape = $geoShape;

        return $this;
    }

    public function getNom(): ?string
    {
        return $this->nom;
    }

    public function setNom(string $nom): self
    {
        $this->nom = $nom;

        return $this;
    }

    public function getNumStation(): ?int
    {
        return $this->numStation;
    }

    public function setNumStation(int $numStation): self
    {
        $this->numStation = $numStation;

        return $this;
    }

    public function getNbBornettes(): ?int
    {
        return $this->nbBornettes;
    }

    public function setNbBornettes(int $nbBornettes): self
    {
        $this->nbBornettes = $nbBornettes;

        return $this;
    }

    public function getEnService(): ?bool
    {
        return $this->enService;
    }

    public function setEnService(bool $enService): self
    {
        $this->enService = $enService;

        return $this;
    }

    public function getStreet(): ?string
    {
        return $this->street;
    }

    public function setStreet(string $street): self
    {
        $this->street = $street;

        return $this;
    }

    public function getMotDirecteur(): ?string
    {
        return $this->motDirecteur;
    }

    public function setMotDirecteur(string $motDirecteur): self
    {
        $this->motDirecteur = $motDirecteur;

        return $this;
    }

    public function getNo(): ?string
    {
        return $this->no;
    }

    public function setNo(string $no): self
    {
        $this->no = $no;

        return $this;
    }

    public function getNrivoli(): ?string
    {
        return $this->nrivoli;
    }

    public function setNrivoli(string $nrivoli): self
    {
        $this->nrivoli = $nrivoli;

        return $this;
    }

    public function getCommune(): ?string
    {
        return $this->commune;
    }

    public function setCommune(string $commune): self
    {
        $this->commune = $commune;

        return $this;
    }

    public function getCodeInsee(): ?string
    {
        return $this->codeInsee;
    }

    public function setCodeInsee(string $codeInsee): self
    {
        $this->codeInsee = $codeInsee;

        return $this;
    }

    public function getLocation2016(): ?string
    {
        return $this->location2016;
    }

    public function setLocation2016(string $location2016): self
    {
        $this->location2016 = $location2016;

        return $this;
    }

    public function getLocation2019(): ?string
    {
        return $this->location2019;
    }

    public function setLocation2019(string $location2019): self
    {
        $this->location2019 = $location2019;

        return $this;
    }
}
